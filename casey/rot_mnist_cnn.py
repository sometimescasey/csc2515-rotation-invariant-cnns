import argparse
import sys
import tensorflow as tf
import time
import numpy as np

# from tensorflow.examples.tutorials.mnist import input_data

FLAGS = None
TRAIN_STEP = 300

## CASEY NOTE: new function to read the .amat
def readAmat(filename):
    array = np.loadtxt(filename)
    # see https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.split.html#numpy-split
    data, labels = np.split(array, [784], axis=1)
    return data, labels

## CASEY NOTE: new function to get batches
## from https://stackoverflow.com/questions/49858574/how-to-implement-correctly-next-batch-in-custom-dataset-in-tensorflow
def next_batch(num, data, labels):
    '''
    Return a total of maximum `num` random samples and labels.
    NOTE: The last batch will be of size len(data) % num
    '''
    num_el = data.shape[0]
    while True: # or whatever condition you may have
        idx = np.arange(0 , num_el)
        np.random.shuffle(idx)
        current_idx = 0
        while current_idx < num_el:
            batch_idx = idx[current_idx:current_idx+num]
            current_idx += num
            data_shuffle = [data[ i,:] for i in batch_idx]
            labels_shuffle = [labels[ i] for i in batch_idx]
            yield np.asarray(data_shuffle), np.asarray(labels_shuffle)

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')


def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')


def main(_):

    print("Loading train data...")
    train_data, train_labels = readAmat("data/mnist_all_rotation_normalized_float_train_valid.amat")
    print("Train data loaded.")
    
    print("Loading test data...")
    test_data, test_labels = readAmat("data/mnist_all_rotation_normalized_float_test.amat")
    print("Test data loaded.")


    # mnist = input_data.read_data_sets(FLAGS.data_dir) # not using this anymore since we have our own rot dataset

    # ---------------- MODEL DEF -------------------
    # any number of inputs, dimension of 784 px each
    x = tf.placeholder(tf.float32, [None, 784])

    # placeholder for predictions
    y_ = tf.placeholder(tf.int64, [None])

    # model params W and b are variables
    W = tf.Variable(tf.zeros([784, 10]))  # matrix mult: maps a 784-item vector into 10 outputs
    b = tf.Variable(tf.zeros([10]))       # just 10 outputs for addition

    # --- 1 --- define our first convolutional layer
    W_conv1 = weight_variable([5, 5, 1, 32])
    #          [patch_size, patch_size, input channels, output channels]
    b_conv1 = bias_variable([32])

    # reshape x to work with tensor
    x_image = tf.reshape(x, [-1, 28, 28, 1])

    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_2x2(h_conv1)
    # after this image is now 14x14

    # --- 2 --- DEFINE 2nd convolutional layer
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])

    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2)+ b_conv2)
    h_pool2 = max_pool_2x2(h_conv2)
    # after this image is now 7x7

    # --- 3 --- DENSELY CONNECTED LAYER
    # fully connected, 1024 neurons

    W_fc1 = weight_variable([7*7*64, 1024])
    # input is 7x7 x 64 channels
    b_fc1 = bias_variable([1024])

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

    # --- 4 --- DROPOUT
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    # --- 5 --- READOUT LAYER: readout to our 10 values in the one-hot label

    W_fc2 = weight_variable([1024, 10])
    b_fc2 = bias_variable([10])

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2


    # --------------- TRAINING STEPS -----------------

    cross_entropy = tf.reduce_mean(tf.losses.sparse_softmax_cross_entropy(labels=y_, logits=y_conv))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    # evaluating our model
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), y_)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # now run it!
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())

    # initialize all variables
    tf.global_variables_initializer().run()

    # run training step many times!

    batch_size = 50
    n_batches = int(len(train_labels) / batch_size)
    print("n_batches: {}".format(n_batches))
    next_batch_generator = next_batch(num=batch_size, data=train_data, labels=train_labels)

    start = time.time()
    for i in range(TRAIN_STEP):
        # batch = mnist.train.next_batch(50) # this is the call to the normal MNIST data set. Not using here
        for j in range(n_batches):
        
            # batch is a 2-tuple of data, label: (batch_size, 784), (batch_size)
            batch_x, _batch_y = next(next_batch_generator) # this is the call to our load from the .amat
            batch_y = _batch_y.squeeze() # i.e. needs to be (50), not (50, 1)
            print(batch_x.shape)
            print(batch_y.shape)
            if i%100 == 0:
                train_accuracy = accuracy.eval(feed_dict={x:batch_x, y_:batch_y, keep_prob: 1.0})
                print("Step %d, training accuracy %g"%(i, train_accuracy))
            train_step.run(feed_dict={x:batch_x, y_:batch_y, keep_prob: 0.5})
    
    end = time.time()
    print("Training time: {} sec".format(end - start))

    # CASEY NOTE: change this to use the test .amat data
    print("test accuracy %g"%accuracy.eval(feed_dict={
        x: test_data, y_: test_labels, keep_prob: 1.0 
    }))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        type=str,
        default='/tmp/tensorflow/mnist/input_data',
        help='Directory for storing input data')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
